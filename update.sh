#!/bin/bash

# echo VM10 Updating
# git add .
# git commit -m "auto update"
# git push origin $1
# # ssh $1@fa22-cs425-2210.cs.illinois.edu "cd ./mp-hangy6-tian23; git pull origin $2; exit"
# # ssh hangy6@fa22-cs425-2210.cs.illinois.edu "cd ./mp1-hangy6-tian23; git checkout hangy6; git fetch --all; git reset --hard origin/main; git pull; exit"
# echo VM10 Updated

for val in {1..9}
do
    echo VM$val Updating
    # ssh $1@fa22-cs425-220$val.cs.illinois.edu "cd; git clone git@gitlab.engr.illinois.edu:hangy6/mp3-hangy6-tian23.git; cd mp3-hangy6-tian23; git checkout dev; git checkout -b hangy6; exit"
    ssh $1@fa22-cs425-220$val.cs.illinois.edu "cd recommended_mp2; git pull origin main; git config credential.helper store; exit"
    # ssh $1@fa22-cs425-220$val.cs.illinois.edu "pip3 install zerorpc; exit"
    # ssh hangy6@fa22-cs425-220$val.cs.illinois.edu "cd ./mp1-hangy6-tian23; git checkout hangy6; git fetch --all; git reset --hard origin/main; git pull; exit"
    echo VM$val Updated
done

echo "All VMs Have Been Updated!"
